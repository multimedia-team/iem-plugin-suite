#!/bin/sh

export LANG=C
DEBDATE=$(date -u -d @$(dpkg-parsechangelog -S Timestamp) +"%b, %d %Y")

projectname() {
  local project
  local proj
  project=$(basename ${1%.jucer})
  proj=$(basename ${project} | tr A-Z a-z)
  echo $project iem-plugin-${proj#iem-plugin-}
}

do_manpage() {
 local p
cat <<EOF
.TH IEM-PLUGIN-SUITE 1 "${DEBDATE}"
.SH NAME
iem-plugin-suite \- Standalone application of the IEMPluginSuite
.SH SYNOPSIS
EOF
for p in "$@"; do
  p=$(basename "$p")
  cat <<EOF
.B ${p}
.LP
EOF
 done
 cat <<EOF
\&...
.SH DESCRIPTION
The \fBIEM Plug-in Suite\fP is an audio plugin suite created at the Institute of
Electronic Music and Acoustics (Graz, Austria).
It features Higher-Order Ambisonic plug-ins (up to 7th order), among them a
number of state of the art encoders, directional compressors, directivity
shapers, delay and reverb effects and analysis tools.

The standalone versions do not have any special cmdline arguments.
.PP
.SH AUTHOR
This manual page was written by IOhannes m zmölnig <umlaeute@debian.org>
for the Debian project (but may be used by others).
EOF
}

rm -rf _man _bin _desktop
mkdir -p _man _bin _desktop


cp resources/Standalone/IEMPluginSuiteSmall.png _desktop/iem-plugin.png

for Standalone in "$@"; do
 test -x "${Standalone}" || continue
 Name=$(basename "${Standalone}")
 name=$(echo ${Name} | tr A-Z a-z)
 standalone=iem-plugin-${name}
 cp "${Standalone}" "_bin/${standalone}"
 # generate a .desktop file for each standalone
 sed \
	-e "s|@Name@|${Name}|g" -e "s|@name@|${name}|g" \
	debian/iem-plugin.desktop.in > "_desktop/${standalone}.desktop"
done

## generate the generic manpage for all plugins
## (which lists all the available plugins)
do_manpage $(for f in _bin/*; do test -e "${f}" && echo $f; done |  sort -u) > _man/iem-plugin-suite.1

## copy the generic manpage as the specific manpage
for x in _bin/*; do
 test -x "${x}" || continue
 cp _man/iem-plugin-suite.1 _man/$(basename "$x").1
done

## drop the generic manpage
rm -f _man/iem-plugin-suite.1
